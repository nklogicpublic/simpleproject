package com.nklogic.testwebproject.simplewebproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimplewebprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimplewebprojectApplication.class, args);
	}

}
